
Gestion du bruit : 
- Etat de l'art des algorithmes de reconnaissance de mélodie

- Objectif : Reconnaitre 1 note puis 1 note + durée
- Recherche sur amplitude du son, fréquence de son, timbre, modélisation de son
- Transformées de Fourier pour extraire les fréquences
- Son bruité 