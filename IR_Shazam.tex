\documentclass[soumission]{ir} % Pour une soumission anonyme
%\documentclass{ir}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{textgreek}
\usepackage{hyperref}
\usepackage[stable]{footmisc}
\usepackage{dirtytalk}

\titre{Reconnaissance de mélodie par analyse spectrale ou fredonnement.}

\auteur{Arnaud Couderc\affil{1} et Alexandre Ludwig\affil{2}}

\affiliation{
    \affil{1}Université de Lorraine\\
    UFR MIM\\
    6, rue de la fontaine, 57710 Aumetz\\
    arnaud.couderc6@etu.univ-lorraine.fr\\
    %
    \affil{2}Université de Lorraine\\
    UFR MIM\\
    29, rue des anémones 57970 Illange\\
    alexandre.ludwig@etu.univ-lorraine.fr
}

\titrecourt{Reconnaissance de mélodie}

\nomcourt{A. Couderc et A. Ludwig}

\encadrant{Dominique Michel}

\resume{L'application Shazam, qui permet de trouver le titre d'une chanson avec un simple enregistrement de quelques secondes, est très populaire du fait de sa puissance et de sa rapidité, créant par ailleurs le verbe \say{shazamer}. Mais ce n'est pas la seule méthode pour retrouver une chanson. Le fredonnement et des données glanées peuvent également nous aider à retrouver le titre d'un morceau.}

\motcle{Song recognition, Music recognition algorithm, signal analysis, Traitement du signal, Humming, métadonnées, }

\begin{document}

\section{Introduction}

Notre façon d'écouter et de consommer de la musique a radicalement changé au fil des années, que ce soit par l'évolution des supports d'écoute ou bien grâce à la numérisation de ceux-ci. Les smartphones nous permettent d'avoir une bibliothèque de plusieurs milliers de morceaux directement dans notre poche. Malgré cela, il est probable que n'importe qui entende une chanson à son goût et aimerait découvrir plus de musiques de l'artiste en question. Les applications de reconnaissance musicale sont nées de ces besoins : Aider l'auditeur à découvrir de nouveaux artistes, et aider les artistes à populariser leurs musiques. La reconnaissance musicale a comme porte-étendard Shazam qui s'appuie sur la représentation mathématique des signaux sonores. Mais \say{shazamer} n'est pas le seul moyen de reconnaître une musique. Nous aborderons une autre technique alliant des fredonnements et des données pour retrouver une chanson avec comme seule aide notre mémoire.

\section[Le son]{Le son\footnote{Cette section provient en grande partie du site revisionsbac.com \nolinkurl{https://www.revisionsbac.com/compte-cours.php?id=101&id2=597}, consulté le 22 mars 2019}}


\subsection{Onde mécanique}

Une \emph{onde mécanique} est un phénomène de propagation d'une perturbation ayant une certaine périodicité dans un milieu. Cette propagation se fait de proche en proche, sans transport de matière mais toujours avec un transport d'énergie.


Il existe deux catégories d'ondes mécaniques : les \emph{ondes transversales} et les \emph{ondes longitudinales}. Elles sont classées en fonction de la direction d'oscillation des particules du milieu par rapport à la direction de propagation.


Une onde est \emph{transversale} si les particules du milieu matériel qu'elle traverse doivent osciller dans une direction perpendiculaire à celle de propagation afin que l'onde se propage.

Une onde est \emph{longitudinale} si les particules du milieu matériel qu'elle traverse doivent osciller dans la même direction que la direction de propagation de l'onde afin que l'onde se propage.

\subsection{Ondes sonores}

Les \emph{ondes sonores} sont des ondes \emph{mécaniques progressives longitudinales}. Elles se propagent dans un milieu matériel de proche en proche par un mécanisme de compression-dilatation des molécules ou atomes du milieu. Ces ondes se propagent dans toutes les directions dans le milieu matériel (on parle d'ondes sphériques).

	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=\textwidth]{images/Ondessonores.jpg} 
		\end{center}
		\caption{Illustration du mécanisme de compression-dilatation d'une onde sonore.}
		\label{fig:Ondessonores}
	\end{figure}

Une source sonore, par exemple un haut-parleur, provoque la mise en vibration de molécules d'air (\emph{figure~\ref{fig:Ondessonores}}). Ces molécules oscillent autour de leur position d'équilibre (leur position de départ) et ce faisant, percutent les molécules d'air voisines qui sont au repos. Celles-ci se mettent à leur tour à osciller et la perturbation (l'onde) se propage ainsi dans toutes les directions. On remarque les différentes zones de compression et de dilatation de l'air. Ce mécanisme est à rapprocher d'un ressort le long duquel une onde longitudinale se propage. Celle-ci se propage de gauche à droite grâce à l'oscillation des spires du ressort, et on observe similairement des zones où les spires sont plus rapprochées (compression) et d'autres où elles sont plus éloignées (dilatation).										
             
\subsection{Hauteur}

La \emph{hauteur} d'un son correspond à sa fréquence sonore. Plus sa fréquence est élevée, plus le son est aigu. Plus sa fréquence est basse, plus le son est grave.

L'oreille humaine peut entendre des sons de 20~\emph{Hz} à 20~\emph{kHz} (20 000~\emph{Hz}) (\emph{figure~\ref{fig:hauteur}})

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=\textwidth]{images/hauteur.jpg} 
    \end{center}
    \caption{Spectre audible par l'oreille humaine.}
    \label{fig:hauteur}
\end{figure}

\subsection{Intensité}

L'intensité d'une onde sonore est une mesure de la puissance surfacique de l'onde à un point donné situé à une distance \emph{r} de la source de l'onde. Il s'agit du rapport entre la puissance sonore de la source et de la surface sur laquelle cette puissance est répartie lorsque l'on se situe à une distance \emph{r} de la source.

\begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.30]{images/intensite.jpg} 
    \end{center}
    \caption{Propagation des ondes sonores.}
    \label{fig:intensite}
\end{figure}

L'intensité sonore est notée \emph{I} et est définie par (\ref{eq:intensite}):

\begin {equation}\label{eq:intensite}
I=\frac{P}{S}=\frac{P}{4 \pi r^{2}} \quad\left(W \cdot m^{-2}\right)
 \end {equation}

 Avec :
 \begin{itemize}
    \item \emph{P} : puissance de la source
    \item \emph{S} : surface sur laquelle la puissance de la source est répartie (ici une sphère de rayon \emph{r}) qui vaut $S=4 \pi r^{2} \left(m^{2}\right)$
 \end{itemize}

 Puisque les ondes sonores se propagent dans toutes les directions, la surface en question est égale à la surface de la sphère de rayon \emph{r}, la puissance sonore de la source s'est répartie sur toute la surface de la sphère (\emph{figure~\ref{fig:intensite}}).

 \subsection{Niveau sonore}

 La perception du volume d'un son se fait en fonction du niveau sonore et non pas directement de l'intensité sonore. En effet, l'oreille humaine n'est pas un récepteur linéaire, mais logarithmique. En conséquence, lorsque l'intensité sonore double, l'oreille ne perçoit pas un stimuli deux fois plus important. Il faut en réalité que l'intensité sonore soit multipliée par 10 pour que le stimuli soit multiplié par 2. Un échelle de niveau sonore est présentée en \emph{figure \ref{fig:niveausonore}}.

 Le niveau sonore est noté \emph{L} et est défini par (\ref{eq:niveausonore}):

 \begin {equation}\label{eq:niveausonore}
L=10 \log \left(\frac{I}{I_{0}}\right) \quad(d B)
 \end {equation}

 Avec :
 \begin{itemize}
    \item \emph{I} : intensité sonore au point où on calcule le niveau sonore
    \item \emph{I}\textsubscript{0} : intensité sonore de référence, $I_{0}=10^{-12}  W \cdot m^{-2}$
 \end{itemize}

 \begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.60]{images/niveausonore.jpg} 
    \end{center}
    \caption{Échelle d'intensité sonore.}
    \label{fig:niveausonore}
\end{figure}

\subsection{Harmoniques}

Lorsque l'on frappe un diapason, on obtient une onde sonore sinusoïdale périodique avec une fréquence unique, valant 440 \emph{Hz}. Cette onde peut-être enregistrée par un micro et affichée sur un écran d'ordinateur (\emph{figure \ref{fig:sonpur}}).

\begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.5]{images/sonpur.jpg} 
    \end{center}
    \caption{Son pur : LA de 440Hz.}
    \label{fig:sonpur}
\end{figure}

On dit que le son correspondant est mono-fréquentiel et on parle souvent de « son pur ».

Si l'on considère maintenant le son produit par un instrument de musique quelconque, on constatera que l'on obtient une onde périodique, mais qui n'est plus sinusoïdale. Par exemple, on peut obtenir un signal de la forme suivante en sommant trois sons pur de fréquences respectives 440\emph{Hz}, 1320\emph{Hz} et 2200\emph{Hz} (\emph{figure \ref{fig:soncomplexe}}).

\begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.5]{images/soncomplexe.jpg} 
    \end{center}
    \caption{Son complexe : Somme de 3 sons purs de 440Hz, 1320Hz et 2200Hz.}
    \label{fig:soncomplexe}
\end{figure}

Ce son est périodique, mais est alors constitué de plusieurs fréquences, ce qui explique que son enveloppe ne soit pas parfaitement sinusoïdale comme dans le cas du son pur.

\subsection{Décomposition en série de \bsc{Fourier}}

Un son complexe périodique est constitué de plusieurs fréquences. La plus basse, notée \emph{f}\textsubscript{1}, est appelée le \emph{fondamental} du son. Les autres fréquences \emph{f}\textsubscript{n} sont des multiples du fondamental et sont appelées \emph{harmoniques} (\ref{eq:harmoniques}).

\begin {equation} \label{eq:harmoniques}
f_{n}=n f_{1} \quad(H z)
 \end {equation}

Sous certaines conditions, une fonction \emph{f(t)} continue et périodique, de période \emph{T}, peut être décomposée en série de \bsc{Fourier}. Elle s'écrit alors (\ref{eq:dsf}):

\begin {equation}\label{eq:dsf}
f(t)=a_{0}+\sum_{n=1}^{+\infty}\left(a_{n} \cos (\omega_{n} t)+b_{n} \sin (\omega_{n} t)\right)
 \end {equation}

 où \textomega~est la pulsation et vaut (\ref{eq:omegan}):

 \begin {equation} \label{eq:omegan}
\omega_{n}=\frac{2 \pi}{T_{n}} \quad(rad\cdot s^{-1})
 \end {equation}
 \newline

 Avec :
 \begin{itemize}
    \item \emph{n} : nombre entier non nul. C'est le rang de l'harmonique.
    \item \emph{f}\textsubscript{1} : fréquence fondamentale
    \newline
 \end{itemize}

 \par Le signal représenté par la \emph{figure~\ref{fig:soncomplexe}} peut-être décomposé (\ref{eq:dec}) en une série de \bsc{Fourier} qui représente alors une somme de signaux sinusoïdaux mono-fréquentiels dont les amplitudes peuvent varier en fonction de l'instrument ayant produit le son.

\begin {equation} 
s(t)=\cos (2 \pi t)+0,5 \cos (6 \pi t)+0,25 \cos (10 \pi t)\label{eq:dec}
 \end {equation}

On peut montrer que toutes les fréquences sont un multiple de la fréquence la plus basse, appelée le fondamental. Les fréquences trouvées sont appelées harmoniques. On dit alors que le son correspondant est multifréquentiel et on parle souvent de « son complexe ».

Son fondamental est alors $f_{1}$ et ses deux harmoniques sont $f_{3}$ et $f_{5}$.

Le précédent signal peut être représenté également par \ref{eq:decplus} :

 \begin {equation} 
s(t)=\cos (2 \pi t)+0,5 \cos (3\cdot2 \pi t)+0,25 \cos (5\cdot2 \pi t)\label{eq:decplus}
 \end {equation}

 Avec (\ref{eq:omega}):

 \begin {equation}\label{eq:omega}
 \omega_{1}=2\pi\quad\omega_{3}=3\cdot\omega_{1}\quad\omega_{5}=5\cdot\omega_{1}\quad(rad\cdot s^{-1})
  \end {equation}

et (\ref{eq:f}):

\begin {equation}\label{eq:f}
f_{1}=440\quad f_{3}=1320\quad f_{5}=2200\quad(H z)
  \end {equation}

 \subsection{Analyse spectrale}

 Pour déterminer la composition fréquentielle d'un son, on peut procéder à une analyse spectrale. Le résultat est un diagramme de l'amplitude de chaque harmonique en fonction de la fréquence et comportant autant de pics que de fréquences présentes. Les hauteurs relatives des pics sont une mesure du « poids » de chaque fréquence dans le son initial.
 Dans le cas d'un son pur (\emph{figure~\ref{fig:sonpur}}), le spectre de fréquence correspondant ne comprend qu'un seul pic de fréquence égale à celle du son pur (\emph{figure \ref{fig:spectresonpur}}).

 \begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.5]{images/spectresonpur.jpg} 
    \end{center}
    \caption{Spectre d'un son pur : LA de 440Hz.}
    \label{fig:spectresonpur}
\end{figure}

 Dans le cas d'un son complexe (\emph{figure~\ref{fig:soncomplexe}}), comportant plusieurs fréquences, le spectre de fréquence présente plusieurs pics. Le pic d'amplitude relative la plus importante correspond à la fréquence du fondamental. Les autres pics correspondent aux fréquences harmoniques et ont une amplitude relative plus faible (ce qui signifie qu'on les perçoit moins) (\emph{figure \ref{fig:spectresoncomplexe}}).

 \begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.5]{images/spectresoncomplexe.jpg} 
    \end{center}
    \caption{Spectre d'un son complexe : Somme de 3 sons purs de 440Hz, 1320Hz et 2200Hz.}
    \label{fig:spectresoncomplexe}
\end{figure}

\subsection{Timbre}

Un son d'une fréquence donnée joué par différents instruments sera perçu différemment par l'oreille humaine. On dit que le timbre du son est différent selon l'instrument considéré.
Physiquement, le timbre du son correspond à la richesse en harmoniques du son joué. La composition spectrale (l'ensemble des harmoniques) varie d'un instrument à l'autre en fonction de la manière dont chaque instrument produit un son (frottement de corde, pincement de corde, vibration d'une anche, vibration d'une colonne d'air, voix, etc.).

\section{Reconnaissance par analyse spectrale}

Shazam\cite{shazamAudioSearch} a été lancé au début des années 2000. Il permettait de reconnaître une chanson grâce à un échantillon de au plus 15 secondes envoyé par appel téléphonique. Le titre et l'artiste de la chanson étaient alors envoyé par SMS à l'utilisateur. Depuis, les smartphones sont arrivés et Shazam est devenu une application, ce qui rendit son utilisation beaucoup plus simple, l'application s'occupant d'enregistrer l'échantillon qu'elle a besoin, de l'envoyer vers un serveur et de donner les détails du morceau recherché. 

\subsection{Théorie}

A chaque fichier audio de la base de données est associé une empreinte unique. Une empreinte partielle est également produite à part de l'échantillon. Cette dernière est comparée à un grand nombre d'empreintes provenant de la base en fonction de :

\begin{itemize}
    \item la~\emph{localité temporelle} dans le morceau,
    \item l'~\emph{invariance de traduction},
    \item la~\emph{robustesse},
    \item l'\emph{entropie}.
 \end{itemize}

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=\textwidth]{images/fingerprint.jpg} 
    \end{center}
    \caption{Principe de fonctionnement de Shazam~\footnotemark[2].}
    \label{fig:fingerprint}
\end{figure}
\footnotetext[2]{La magie de Shazam : dans les entrailles de l’algorithme.~\nolinkurl{https://lesnumeriques.com/audio/magie-shazam-dans-entrailles-algorithme-a2375.html}, consulté le 20 janvier 2019\cite{num}}

\par La \emph{localité temporelle} signifie que le hash de l'empreinte est calculée autour d'un certain instant \emph{t}, pour qu'il n'y ait pas d'événements perturbateurs lors du calcul de l'empreinte. L'\emph{invariance de traduction} désigne la reproductibilité d'un hash d'une empreinte tant que la \emph{localisation temporelle} du hash est contenue dans la morceau. Un échantillon peut en effet provenir de n'importe quel endroit de la chanson. La \emph{robustesse} signifie que les hashs d'une empreinte doit être reproductible, pour un instant donné et une période donnée, à partir d'une copie dégradée du morceau. L'\emph{entropie} doit être suffisamment haute pour minimiser le risque d'appariement entre des emplacements qui ne se correspondent pas entre l'échantillon et les morceaux dans la base. Une entropie trop faible provoque trop de disparités d'appariement et une entropie trop élevée accroit la fragilité et la non-reproductibilité d'un hash d'empreinte en présence de bruit ou de distorsion.

Il fallait arriver à se concentrer sur certains points spécifiques. Il se trouve que les spectrogrammes permettent de fixer certains points. Ce graphique à trois dimensions donne l'intensité d'un son en fonction de sa fréquence (en \emph{Hz}) et de son instant \emph{t} dans le morceau (\emph{figure~\ref{fig:spectrogramme}}).

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=\textwidth]{images/spectrogramme.jpg} 
    \end{center}
    \caption{Spectrogramme et constellation\footnotemark[2].}
    \label{fig:spectrogramme}
\end{figure}

Un spectrogramme est en réalité une représentation graphique de la série de \bsc{Fourier} associée à une chanson, les points représentant l'intensité d'une fréquence à un instant \emph{t}, intensité qui baisse dans le temps. Ce spectrogramme peut être réduit à un ensemble de coordonnées, cette réduction, appelée constellation, est insensible aux égalisateurs (l'intensité sera modifiée mais elle sera toujours plus ou moins marquée). Pour une zone de temps définie dans une chanson, les constellations doivent correspondre.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=\textwidth]{images/constellation.jpg} 
    \end{center}
    \caption{Constitution de marqueurs temporels\footnotemark[2].}
    \label{fig:constellation}
\end{figure}

Cette constellation est transformée en empreinte par la sélection de points d'ancrages associés à une zone cible chacun (figure~\ref{fig:constellation}). Pour chaque point dans la zone cible, on forme un triplet (fréquence au point d'ancrage, fréquence du point de la zone cible, différence de temps) sous la forme (\ref{eq:ancre}) :

\begin {equation}\label{eq:ancre}
(f_{1}; f_{2}; \Delta t)
  \end {equation}

Ce triplet constitue ce que l'on appelle un marqueur. Ce marqueur est ensuite haché et forme ensuite avec l'instant \emph{t} du point d'ancrage un marqueur temporel (\ref{eq:marq}), les mêmes qui seront comparés à ceux de l'échantillon.

\begin {equation}\label{eq:marq}
[(f_{1}; f_{2}; \Delta t); t]
  \end {equation}

Par exemple, le marqueur temporel (\ref{eq:marqex}) représente le couple de points dont le premier a une fréquence de 523 \emph{Hz}, le second de 659 \emph{Hz}, séparés de 27 \emph{ms} et dont le premier arrive à la 37e seconde du morceau (37 000 \emph{ms}).

\begin {equation}\label{eq:marqex}
[(523; 659; 27); 37000]
  \end {equation}

Une empreinte est une enveloppe contenant tous les marqueurs temporels sous forme de hash ainsi que les informations du morceau (artiste, nom de la chanson, nom de l'album, année de sortie...). La structure d'une empreinte est stockée sur 64 bits, 32 bits pour le hash et 32 bits pour les informations sur le morceau ainsi que son décalage par rapport au début du morceau. Les empreintes sont stockées selon la valeur de leur hash pour accélérer le traitement.

Le nombre de hashs\footnote[3]{marqueurs} calculés par seconde est environ égal à la densité de la constellation fois l'étirement de la zone cible. Si cet étirement vaut $F=10$, alors le nombre de hashs sera sera environ égal à F fois le nombre de points de la constellation. Cet étirement permet de réduire considérablement le nombre de marqueurs temporels, par contre l'espace de stockage nécessaire s'en trouve fortement impacté. Former des marqueurs accélère également grandement la recherche par rapport à l'utilisation des points de la constellations seuls. Avec une fréquence prenant 10 bits, et un décalage \textDelta t de 10 bits, un marqueur représente 30 bits, par rapport à un seul point (10 bits). La spécificité du hash est alors un million de fois plus grande, et la recherche d'un marqueur est un alors accélérée. Avec la génération combinatoire des hashs, il y a F fois le nombre de marqueurs temporels dans l'échantillon à rechercher et F fois le nombre de marqueurs dans la base, ce qui porte le facteur accélération à environ $1 000 000/F^{2}$ soit 10000 par rapport à un recherche basée sur les points de la constellation.

Le hachage porte la probabilité de la survie d'un point au carré. Si on considère $p$ la probabilité qu'un pic d'un spectrogramme survive du microphone du mobile jusqu'à l'échantillon reçu, la probabilité qu'un hash survive est $p^{2}$. Cette réduction de la survivabilité d'un hash est un compromis pour encore gagner en rapidité. En effet, la réduction de cette probabilité est atténuée par le grand nombre de hashs calculés. Si $F=10$, la probabilité qu'un hash survive pour un point d'ancrage donné serait la probabilité qu'un point d'ancrage et au moins un point de sa zone cible survivent. La probabilité qu'un moins un hash par point d'ancrage survive est $p*(1-(1-p)^{F})$. Avec $F>10$ et $p>0.1$, on a (\ref{eq:prob}), ce qui n'est pas si mal.

\begin {equation}\label{eq:prob}
p \simeq p\cdot(1-(1-p)^{F}) \simeq p^{2}
  \end {equation}

Par différentes techniques, la rapidité de traitement a été multiplié par 1000 pour un coût de stockage 10 fois plus grand et une petite perte de probabilité de détection. La valeur de l'étirement peut être modifiée selon la qualité audio, pour les mobiles, on préférera l'augmenter un peu, ce qui aura un impact.

\subsection{Pratique}

Un échantillon envoyé depuis un téléphone mobile est loin d'être exempt de défauts : bruit, instabilité du réseau téléphonique puis cellulaire... La source audio est peut-être soumise à une certaine distorsion, l'environnement peut-être trop ouvert ou trop fermé et générer de la réverbération. L'échantillon transmis par l'utilisateur contient tous ces éléments, et une analyse globale de l'échantillon est impossible, même en travaillant avec des empreintes. Ce sont les marqueurs temporels qui sont alors utilisés et comparés à tous ceux qui sont dans la base de données, de façon à trouver le plus grand nombre de marqueurs \say{semblables} à un décalage temporel près.

Pour chaque marqueur de l'échantillon, on cherche dans la base des marqueurs qui ont les mêmes attributs (fréquence au point d’ancrage, fréquence du point de la zone cible, différence de temps). Pour chaque marqueur, on regarde les informations de l'empreinte du marqueur et un espace du nom du titre de la chanson est créé. Dans cet espace, les couples de marqueurs \say{semblables} sont triés selon leur décalage entre les instants de chaque marqueur\footnote[4]{L'échantillon ne commence jamais au début du morceau, il est relatif} (\emph{t}\textsubscript{1} et \emph{t}\textsubscript{2}). Le but est de trouver un marqueur ayant le même décalage (\textDelta t) que celui de l'échantillon. Cette recherche de correspondance s'effectue en approximativement $N^{*} \log (N)$ fois, N étant le nombre de points du nuage (\emph{figures \ref{fig:nuagenomatch} et \ref{fig:nuagematch}}).

Supposons un morceau dont le refrain commence à la 56\textup{ème} seconde, un marqueur aura comme instant \emph{t} associé 56 \emph{s}. Supposons maintenant un échantillon capté à partir de la 30\textup{ème} seconde, le marqueur aura un instant \emph{t} associé décalé de 30 \emph{s}.

Deux morceaux peuvent être détectés comme \say{semblables} alors que ce n'est pas le cas, pour de multiples raisons (même tonalité/progression harmonique, tons voisins, reprise...). Dans ce cas, les 2 marqueurs auront une fréquence au point d’ancrage, une fréquence du point de la zone cible et une différence de temps identiques mais un instant \emph{t} qui ne correspondra pas. Pour les autres marqueurs communs, les décalages temporels ne correspondront pas (\emph{figure~\ref{fig:nuagenomatch} et \ref{fig:histogrammenomatch}}).

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=\textwidth]{images/nuagenomatch.jpg} 
    \end{center}
    \caption{Nuage de points des coordonnées temporelles des marqueurs assortis : pas de diagonale\footnotemark[2]}
    \label{fig:nuagenomatch}
\end{figure}

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=\textwidth]{images/histogrammenomatch.jpg} 
    \end{center}
    \caption{Histogramme des décalages temporels : pas de valeur significative\footnotemark[2].}
    \label{fig:histogrammenomatch}
\end{figure}

Dans le cas ou les deux morceaux correspondent, la plupart\footnote[5]{Les marqueurs restants sont dûs à l'environement sonore.} des couples de marqueurs assortis auront le même décalage (30 \emph{s} dans l'exemple). Cela se traduit dans le nuage (\emph{figure~\ref{fig:nuagematch}}) par une agrégation de la majorité des points sur une droite passant par l'abscisse 30 de pente 1\footnote[6]{Cette pente peut être modifiée en cas d'accélération du morceau.}. Cette droite traduit le décalage de 30 \emph{s} de l'échantillon visible sur la \emph{figure~\ref{fig:histogrammematch}}.

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=\textwidth]{images/nuagematch.jpg} 
    \end{center}
    \caption{Nuage de points des coordonnées temporelles des marqueurs assortis : existence d'une diagonale (Le point rouge représente le marqueur de l'exemple)\footnotemark[2]}
    \label{fig:nuagematch}
\end{figure}

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=\textwidth]{images/histogrammematch.jpg} 
    \end{center}
    \caption{Histogramme des décalages temporels : décalage de 30 secondes pour une grande partie des marqueurs\footnotemark[2].}
    \label{fig:histogrammematch}
\end{figure}

L'algorithme est plutôt résistant à un certain niveau de bruit et même à la distorsion. Il évite les voix, le trafic, les coupures et les autres musiques. Les histogrammes de nuages de points sont insensibles aux discontinuités, les coupures ou les interférences ne posent donc aucun problème. Plus encore, la reconnaissance de plusieurs morceaux à travers un seul échantillon est possible. Ce phénomène est appelé \emph{transparence}.

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=\textwidth]{images/recratenoise.png} 
    \end{center}
    \caption{Taux de reconnaissance : Bruit ajouté. Le taux de reconnaissance devient satisfaisant à partir d'un rapport signal sur bruit de 0 \emph{dB}\cite{shazamAudioSearch}.}
    \label{fig:recratenoise}
\end{figure}

Une étude a été menée en lançant 250 reconnaissances de longueur différente, avec un bruit ambiant plus ou moins haut et une base de données de 10 000 morceaux. Des échantillons de 5, 10 et 15 secondes ont été pris au milieu de chaque chanson de test et toutes les chansons sont présentes dans la base. Pour chaque échantillon, la puissance du bruit à été normalisée pour atteindre le rapport signal sur bruit désiré. Les échantillons ont été produit en mono, 8 \emph{kHz} et en résolution de 16 bits La \emph{figure~\ref{fig:recratenoise}} présente les résultats avec le bruit ajouté et la \emph{figure~\ref{fig:recratenoisegsm}} représente les résultats avec le bruit ajouté et une compression GSM.

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=\textwidth]{images/recratenoisegsm.png} 
    \end{center}
    \caption{Taux de reconnaissance : Bruit ajouté + compression GSM. Le taux de reconnaissance devient satisfaisant à partir d'un rapport signal sur bruit de 6 \emph{dB}\cite{shazamAudioSearch}.}
    \label{fig:recratenoisegsm}
\end{figure}

\section{Reconnaissance par métadonnées et fredonnement}

On peut ne pas pouvoir prélever un échantillon de la musique que l'on a entendu. Cela peut être frustrant d'avoir un morceau en tête et de ne rien savoir à son sujet. Dans ce cas, le fredonnement peut être une technique de reconnaissance de mélodie, moins précise que celle employée par Shazam\cite{shazamAudioSearch}.
C'est dans cette idée qu'a été développé HQMS\cite{hqms} (Hybrid Query by Humming and Metadata Search System), un système de reconnaissance musicale basé sur le fredonnement et les métadonnées, pour avoir de meilleurs résultats.

\begin{figure}[H]
    \begin{center}
        \includegraphics[scale=0.35]{images/hqmssearch.png}
    \end{center}
    \caption{Architecture de HQMS\cite{hqms} : On donne d'abord des métadonnées puis on fredonne.}
    \label{fig:hqmssearch}
\end{figure}


\begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.35]{images/hqmsinterface.png}
    \end{center}
    \caption{Interface de HQMS\cite{hqms} de saisie de métadonnées : On peut ajuster la pertinence d'une information en lui affectant un poids.}
    \label{fig:hqmsinterface}
\end{figure}

\subsection{Métadonnées}

 Les métadonnées sont envoyées à un filtre composé d'une multitude de sous-filtres. L'utilisateur est invité à donner toutes les informations dont il dispose. S'il ne possède aucune information à part l'air du morceau, le système s'appuiera seulement sur le fredonnement. Statistiquement, les utilisateurs arrivent à fournir généralement entre une et deux informations. Les résultats de ce filtre sont stockés temporairement pour être utilisé plus tard par le filtre de fredonnement (\emph{figure~\ref{fig:hqmssearch}}).


Cinq types de métadonnées sont prises en compte parmi le nom de l'album, le nom de l'artiste, le genre, le titre du morceau et l'année de sortie. L'utilisateur peut attribuer un poids à chaque donnée saisie s'il est plus ou moins sûr de l'information qu'il donne~\emph{figure~\ref{fig:hqmsinterface}}. Cette idée d'attribution de poids à une information a été motivée par ce scénario :

\say{\emph{Quand un utilisateur veut retrouver une chanson, s'il ne se souvient pas des paroles mais de la mélodie ou de certaines données, il peut alors classer les informations en fonction de leur pertinence.}}

MySQL est utilisé pour stocker les métadonnées et les valeurs de similarité. Lorsque l'utilisateur envoie une requête pondérée, chaque donnée se compare aux métadonnées correspondantes de chaque chanson en base en utilisant une fonction de Levenshtein\footnote[7]{La distance de Levenshtein est une distance donnant une mesure de la différence entre deux chaînes de caractères. Elle est égale au nombre minimal de caractères qu'il faut supprimer, insérer ou remplacer pour passer d’une chaîne à l’autre~\nolinkurl{https://fr.wikipedia.org/wiki/Distance_de_Levenshtein}, consulté le 20 avril 2019.}. Après comparaison, chaque valeur est multiplié par le poids donné par l'utilisateur (\emph{figure~\ref{fig:hqmsfilter}}).

\begin{figure}[H]
    \begin{center}
        \includegraphics[scale=0.25]{images/hqmsfilter.png}
    \end{center}
    \caption{Fonctionnement du filtrage de métadonnées\cite{hqms}.}
    \label{fig:hqmsfilter}
\end{figure}

\subsection{Fredonnement}

Le fredonnement de l'utilisateur est transformé en \emph{série temporelle}. Ces séries temporelles sont comparées à celles des morceaux trouvés par le filtrage des métadonnées. Si aucune métadonnée n'a été fournie, le résultat sera celui d'un simple système de reconnaissance de fredonnement. Le premier filtre réduit le champ de recherche et le second accroît la pertinence des résultats.

C'est le projet open source CompariSong\footnote[8]{\nolinkurl{https://www.openhub.net/p/comparisong}, consulté le 15 avril 2019.} qui a été utilisé pour filtrer les fredonnements. CompariSong convertit le fichier audio en segments de 10 secondes. A partir de la fréquence fondamentale et la puissance audio de l'extrait, les séries temporelles sont converties en lettres. Le même traitement est effectué sur le fredonnement de l'utilisateur. Pour trouver des correspondances avec les fichiers indexés, la distance de Levenshtein est utilisée. Les résultats récupérés sont présentés dans une liste triée, de sorte que les plus pertinents se trouvent en haut.

\section{Expérience}

Des mesures ont été effectuées sur un ensemble de 35 chansons. Trois catégories d'âges différents ont été sélectionnés. La première tranche d'âge est comprise entre 10 et 15 ans, la deuxième entre 16 ans et 20 ans et la troisième entre 21 ans et 25 ans. Pour chaque tranche d'âge, une personne de chaque sexe a été choisie. En effectuant des mesures statistiques, une mesure du pourcentage de précision a été trouvé : (\emph{\ref{eq:prec}}).

\begin {equation}\label{eq:prec}
\%=\frac{(N-P)+1}{N} \times 100
 \end {equation}

 Avec :

 \begin{itemize}
     \item N : le nombre total de chansons
     \item P : la position de la chanson pertinente dans la liste des résultats
 \end{itemize}

 Cette formule permet de trouver la précision du système de requête par fredonnement, si une chanson se retrouve première, la précision est de 100\%.

 \begin{figure}[h]
    \begin{center}
        \includegraphics[width=\textwidth]{images/hqmssexe.png}
    \end{center}
    \caption{Moyennes des résultats des requêtes effectuées pour une voix d'homme et une voix de femme\cite{hqms}.}
    \label{fig:hqmssexe}
\end{figure}

Les femmes ont un ton aigu par rapport aux hommes, alors au départ, l'ensemble de données a été divisé en deux groupes d'hommes et de femmes pour une évaluation basée sur le genre et les chansons sélectionnées sont des solos chantés par un homme ou une femme. Pour chaque chanson initialement, le groupe masculin a enregistré la requête, puis le groupe féminin a enregistré la requête. Les moyennes des deux groupes ont été comparées pour chaque chanson. La \emph{figure~\ref{fig:hqmssexe}} montre les comparaisons de précisions des résultats pour les chansons chantées par un homme ou une femme. Sans surprise, les fredonnements d'une personne ayant le même sexe que l'interprète de la chanson recherchée produisent des résultats plus précis. Les différences à propos de l'âge sont, elles, surprenantes. Les personnes de la première tranche d'âge produisent de bons résultats sur les voix féminines. Ceci est dû à la voix des enfants, sensiblement aussi aiguë que celle des femmes (\emph{figure~\ref{fig:hqmsage}}).

\begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.3]{images/hqmsage.png}
    \end{center}
    \caption{Moyennes des résultats des requêtes effectuées sur une voix de femmes par toutes les catégories d'âge : les enfants produisent des résultats proches de ceux des femmes\cite{hqms}.}
    \label{fig:hqmsage}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ce système peut être utilisé comme détecteur de violation de droit d'auteur, pour détecter si une personne chante la chanson d'un autre sans en avoir la permission. C'est surement ce qui est utilisé sur YouTube ou chez d'autres autorités de vérification de droits d'auteurs.

\section{Conclusion}

Nous avons analysé deux méthodes de reconnaissance musicale plutôt différentes de par leurs approches. La première, qui est utilisé par l'application Shazam permet, grâce à un échantillon pris depuis n'importe quel endroit d'un morceau, de retrouver des similitudes avec une partie d'une chanson dans sa base de données, et de retrouver le titre de cette chanson qui passe en boucle. La seconde approche consiste à allier un fredonnement d'un titre entendu et certaines données pondérées telles que le genre du morceau ou son année de parution pour en retrouver le titre. Si nous devions en choisir une seule, nous ne pourrions pas vraiment : ces deux approches sont en quelque sorte complémentaires, et l'une sera utile là ou l'autre le sera un peu moins. Néanmoins, ces méthodes de reconnaissance ne pourraient pas servir qu'à la reconnaissance musicale. Peut-être que ces algorithmes peuvent être repris dans des outils de reconnaissance de contenu soumis au droit d'auteur, ou à la recherche de l'identité d'une personne à travers son empreinte vocale.

\remerciement{Nous tenons à remercier M. Michel qui nous a enseigné tout ce qu'il savait. Nous remercions aussi le site revisionsbac.com\footnote[9]{\nolinkurl{https://www.revisionsbac.com}, consulté le 22 mars 2019} pour son cours complet sur les ondes, en particulier sonores. Nous tenions enfin à remercier le site lesnumeriques.com\footnote[10]{\nolinkurl{lesnumeriques.com}, consulté le 14 avril 2019} pour son explication du fonctionnement de Shazam.}

\appendix

\section*{Annexes}

\subsection*{Sujet : Reconnaissance de mélodie}

Tout le monde connaît la célèbre application Shazam disponible sur smartphones. Celle-ci permet d'identifier en temps réel un morceau de musique ou une chanson, même en présence d'un bruit de fond. Comment fonctionne-t-elle ? Il est demandé d'effectuer une étude bibliographique sur les algorithmes de reconnaissance de musiques. Cette tâche sera suivie de la  sélection d'un algorithme puis du développement d'une maquette basée sur celui-ci.

\emph{Nombre d’étudiants} : 2

\emph{Remarques} :

Les algorithmes de reconnaissance de sons  sont basés sur l'analyse (très gourmande en temps machine) de signaux audio échantillonnés. Le développement sera donc effectué en C++.

\emph{Mots clés} : Song recognition, Music recognition algorithm, signal analysis. Traitement du signal.

\subsection*{Historique des réunions}

Vendredi 25 décembre 2018 : première réunion définissant le but du sujet et les premiers axes de recherche.

Vendredi 1\textsuperscript{er} février 2019 : présentation des premiers travaux de recherche.

Jeudi 7 février 2019 : Cours particulier sur les ondes (sonores).

\bibliographystyle{unsrt}
\bibliography{IR_biblio}

\end{document}
