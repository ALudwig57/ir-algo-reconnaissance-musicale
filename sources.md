
Sources IR (Shazam)

- An Industrial-Strength Audio Search Algorithm : Avery Li-Chun Wang (avery@shazamteam.com)
http://www.ee.columbia.edu/~dpwe/papers/Wang03-shazam.pdf

- Traduction de l'article du dessus par Les numériques : https://www.lesnumeriques.com/audio/magie-shazam-dans-entrailles-algorithme-a2375.html

- Hybrid Query by Humming and Metadata Search System (HQMS) Analysis over Diverse Features : https://thesai.org/Downloads/Volume2No9/Paper%2011%20-%20Hybrid%20Query%20by%20Humming%20and%20Metadata%20Search%20System%20(HQMS)%20Analysis%20over%20Diverse%20Features.pdf

- Explication par Doug Stevenson : https://www.quora.com/How-does-the-SoundHound-application-technically-work
  
- Qu'est-ce-que le son ? Caractéristiques des ondes acoustiques (Dossier) : https://www.easyzic.com/dossiers/qu-est-ce-que-le-son-caract%C3%A9ristiques-des-ondes-acoustiques,d2.html

- Cours sur les ondes mécaniques et sur les ondes sonores : https://www.revisionsbac.com/compte-cours.php?id=101&id2=597
  
- Midomi : Comme Shazam, mais en sifflant : https://www.midomi.com/ (SoundHound pour mobiles)

- Robust and invariant audio pattern matching - WANG, Avery Li-Chun and CULBERT, Daniel:  https://drgoulu.files.wordpress.com/2009/07/wo03091990a1.pdf
  
- The Shazam music recognition service, Avery WANG
  
- Caractérisation d'un air de musique et reconnaissance avec un son dans une base de données, Julien Osmalskyj : http://www.montefiore.ulg.ac.be/services/acous/STSI/file/tfe_osmalskyj.pdf
  
- Cours Transformation de Fourier : https://www.math.u-bordeaux.fr/~smarques/cours/Physique/exosup/fourier.pdf
  
- An Implementation of Fast Fourier Transform : https://github.com/lzhbrian/Fast-Fourier-Transform