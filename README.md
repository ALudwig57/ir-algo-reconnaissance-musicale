---
bibliography:
- 'IR\_biblio.bib'
---

Introduction
============

La reconnaissance musicale, qui a comme porte-étendard Shazam, s'appuie
sur la représentation des signaux sonores, qui sont des signaux
périodiques. La représentation d'un son se nomme le spectre. C'est qui,
grâce à son concept de série de , a permis de représenter un son de
manière mathématique.

Consignes générales
===================

Les soumissions seront anonymes et se feront par voie électronique
exclusivement à partir du site web de la conférence. Elles devront être
soumises au format `pdf` exclusivement et devront impérativement
utiliser le format LaTeX `ir`. Les soumissions qui dépasseraient 20
pages ou qui ne respecteront pas l'anonymat et/ou le format LaTeX ne
seront pas évaluées. Les articles peuvent être soumis en français ou en
anglais.

Section de niveau 2
-------------------

Notes en bas de page
--------------------

Les notes en bas de page[^1] s'utilisent de cette façon. En particulier
pour citer une URL comme référence, il faut utiliser une note en bas de
page, en indiquant la date de consultation du document en ligne[^2].

Je cite [@shazamAudioSearch] et je cite [@robustAudioMatching] et je
cite [@airMusique]

Conclusion
==========

Le document doit se terminer par une conclusion.

Annexe {#annexe .unnumbered}
======

[^1]: Première note en bas de page

[^2]: <https://www.latex-project.org/help/documentation/>, consulté le
    30 mai 2018
